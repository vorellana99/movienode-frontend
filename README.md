# MovieNode - Frontend

### Desarrollo de una Aplicación Web con operaciones de Películas: Buscar, Listar y Reemplazar. Consumiendo la API desarrollada en el proyecto **MovieNode - Backend** (https://movienode-backend.azurewebsites.net/swagger)
### Desarrollado con React (versión 18.0) 

### La aplicación se encuentra desplegada en el servicio de **Azure** y puede acceder inmediatamente a la aplicación con el siguiente enlace:
### https://white-desert-0bbcf0b0f.1.azurestaticapps.net/

![alt text](https://gitlab.com/vorellana99/movienode-frontend/-/raw/main/resources/frontend-1.png)
## 1. Características
* Consumo de la API **MovieNode - Backend ** https://movienode-backend.azurewebsites.net/swagger .
* **Buscador de películas:** Busca por el título de la película y opcionalmente por el año.
* **Obtener todas las películas:** Presenta 5 registros por página.
* **Buscar y reemplazar:** Presenta el Plot anteior y el nuevo para verificar el cambio.
* Mensajes en ventanas modales.
* Muestra los mensajes que vienen desde la API.
* Loader de carga para cada solicitud.
* Diseño responsivo (Se adapta a cualquier tamaño de pantalla).
* Despliegue y publicación en el servicio Azure.
* El código del proyecto ha sido analizado a través de ESLint (correción y buenas prácticas de código) y no arrojo error o adevertencia alguno.

## 2. Tecnologías de desarrollo
Para el presente proyecto se utilizarón las siguientes tecnologías como librerías, frameworks, servicios en la nube, herramientas de despliegue entre otros.

### Frontend
* **React:** Librería para crear aplicaciones Web SPA.
* **Axios:** Librería para realizar operaciones como cliente HTTP.
* **Bootstrap:** Librería CSS para facilitar el uso de estilos.
* **react-paginate:** Librería para el manejo de paginaciones.

### Deployment
*  **Azure**: Servicio en la nube que se utilizó para desplegar la aplicación.
*  **GitLab**: Servicio de repositorio de código fuente en donde se encuentran almacenado todo el código del proyecto.

## 3. Instalación y uso

```sh
# descargamos el proyecto
git clone https://gitlab.com/vorellana99/movienode-frontend.git

# entramos a la carpeta del repositorio
cd movienode-frontend

# instalamos los módulos requeridos
npm install

# iniciamos la aplicación
npm run start

# en el navegador ingresamos la siguiente URL
http://localhost:3000/
```

## Gracias